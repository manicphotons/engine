#include "../api/platform.h"
#include "../api/window.h"
#include "../api/keyboard.h"
#include "../api/timer.h"

// NOTE: I want to use ascii versions of functions - not unicode versions.
//       Therefore I undef the UNICODE macro before including the windows header.
//       This may change once I have correctly written the platform and window code for X11 and Wayland too.
#undef UNICODE
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdbool.h>
#include <stdint.h>

void TIMER_Initiate(struct TIMER_State* timer) {
	if(!timer) return;

	timer->frequency = 0.0;
	timer->previous  = 0.0;
	timer->current   = 0.0;
	timer->elapsed   = 0.0;

	LARGE_INTEGER frequency = {0};
	if(!QueryPerformanceFrequency(&frequency)) return;
	
	LARGE_INTEGER initial = {0};
	if(!QueryPerformanceCounter(&initial)) return;

	timer->frequency = (double)frequency.QuadPart;
	timer->previous  = (double)initial.QuadPart;
	timer->current   = (double)initial.QuadPart;
	timer->elapsed   = 0.0;
}

void TIMER_Update(struct TIMER_State* timer) {
	if(!timer) return;

	timer->previous = timer->current;
	timer->elapsed   = 0.0;
	if(timer->frequency == 0) return;

	LARGE_INTEGER reading = {0};
	if(!QueryPerformanceCounter(&reading)) return;

	timer->current = (double)reading.QuadPart;
	timer->elapsed = (timer->current - timer->previous)/timer->frequency;
}

void TIMER_Terminate(struct TIMER_State* timer) {
	if(!timer) return;

	timer->frequency = 0.0;
	timer->previous  = 0.0;
	timer->current   = 0.0;
	timer->elapsed   = 0.0;
}


uint8_t KEYBOARD_ConvertKeycode(uint16_t code) {
	switch(code) {
		case VK_RETURN:     return KEYBOARD_Enter;
		case VK_ESCAPE:     return KEYBOARD_Escape;
		case VK_BACK:       return KEYBOARD_Backspace;
		case VK_TAB:        return KEYBOARD_Tab;
		case VK_SPACE:      return KEYBOARD_Spacebar;
		case VK_CAPITAL:    return KEYBOARD_CapsLock;
		case VK_INSERT:     return KEYBOARD_Insert;
		case VK_DELETE:     return KEYBOARD_Delete;
		case VK_PRIOR:      return KEYBOARD_PageUp;
		case VK_NEXT:       return KEYBOARD_PageDown;
		case VK_HOME:       return KEYBOARD_Home;
		case VK_END:        return KEYBOARD_End;
		case VK_UP:         return KEYBOARD_Up;
		case VK_DOWN:       return KEYBOARD_Down;
		case VK_LEFT:       return KEYBOARD_Left;
		case VK_RIGHT:      return KEYBOARD_Right;
		case VK_LSHIFT:     return KEYBOARD_LeftShift;
		case VK_RSHIFT:     return KEYBOARD_RightShift;
		case VK_LCONTROL:   return KEYBOARD_LeftControl;
		case VK_RCONTROL:   return KEYBOARD_RightControl;
		case VK_LMENU:      return KEYBOARD_LeftAlt;
		case VK_RMENU:      return KEYBOARD_RightAlt;
		case VK_LWIN:       return KEYBOARD_LeftSuper;
		case VK_RWIN:       return KEYBOARD_RightSuper;
		case 0x41:          return KEYBOARD_LetterA;
		case 0x42:          return KEYBOARD_LetterB;
		case 0x43:          return KEYBOARD_LetterC;
		case 0x44:          return KEYBOARD_LetterD;
		case 0x45:          return KEYBOARD_LetterE;
		case 0x46:          return KEYBOARD_LetterF;
		case 0x47:          return KEYBOARD_LetterG;
		case 0x48:          return KEYBOARD_LetterH;
		case 0x49:          return KEYBOARD_LetterI;
		case 0x4A:          return KEYBOARD_LetterJ;
		case 0x4B:          return KEYBOARD_LetterK;
		case 0x4C:          return KEYBOARD_LetterL;
		case 0x4D:          return KEYBOARD_LetterM;
		case 0x4E:          return KEYBOARD_LetterN;
		case 0x4F:          return KEYBOARD_LetterO;
		case 0x50:          return KEYBOARD_LetterP;
		case 0x51:          return KEYBOARD_LetterQ;
		case 0x52:          return KEYBOARD_LetterR;
		case 0x53:          return KEYBOARD_LetterS;
		case 0x54:          return KEYBOARD_LetterT;
		case 0x55:          return KEYBOARD_LetterU;
		case 0x56:          return KEYBOARD_LetterV;
		case 0x57:          return KEYBOARD_LetterW;
		case 0x58:          return KEYBOARD_LetterX;
		case 0x59:          return KEYBOARD_LetterY;
		case 0x5A:          return KEYBOARD_LetterZ;
		case 0x31:          return KEYBOARD_Number1;
		case 0x32:          return KEYBOARD_Number2;
		case 0x33:          return KEYBOARD_Number3;
		case 0x34:          return KEYBOARD_Number4;
		case 0x35:          return KEYBOARD_Number5;
		case 0x36:          return KEYBOARD_Number6;
		case 0x37:          return KEYBOARD_Number7;
		case 0x38:          return KEYBOARD_Number8;
		case 0x39:          return KEYBOARD_Number9;
		case 0x30:          return KEYBOARD_Number0;
		case VK_F1:         return KEYBOARD_Function01;
		case VK_F2:         return KEYBOARD_Function02;
		case VK_F3:         return KEYBOARD_Function03;
		case VK_F4:         return KEYBOARD_Function04;
		case VK_F5:         return KEYBOARD_Function05;
		case VK_F6:         return KEYBOARD_Function06;
		case VK_F7:         return KEYBOARD_Function07;
		case VK_F8:         return KEYBOARD_Function08;
		case VK_F9:         return KEYBOARD_Function09;
		case VK_F10:        return KEYBOARD_Function10;
		case VK_F11:        return KEYBOARD_Function11;
		case VK_F12:        return KEYBOARD_Function12;
		case VK_F13:        return KEYBOARD_Function13;
		case VK_F14:        return KEYBOARD_Function14;
		case VK_F15:        return KEYBOARD_Function15;
		case VK_F16:        return KEYBOARD_Function16;
		case VK_F17:        return KEYBOARD_Function17;
		case VK_F18:        return KEYBOARD_Function18;
		case VK_F19:        return KEYBOARD_Function19;
		case VK_F20:        return KEYBOARD_Function20;
		case VK_F21:        return KEYBOARD_Function21;
		case VK_F22:        return KEYBOARD_Function22;
		case VK_F23:        return KEYBOARD_Function23;
		case VK_F24:        return KEYBOARD_Function24;
		case VK_OEM_3:      return KEYBOARD_Backtick;
		case VK_OEM_4:      return KEYBOARD_LeftSquare;
		case VK_OEM_6:      return KEYBOARD_RightSquare;
		case VK_OEM_2:      return KEYBOARD_ForeSlash;
		case VK_OEM_5:      return KEYBOARD_BackSlash;
		case VK_ADD:        return KEYBOARD_Equals;
		case VK_SUBTRACT:   return KEYBOARD_Subtract;
		case VK_OEM_1:      return KEYBOARD_SemiColon;
		case VK_OEM_COMMA:  return KEYBOARD_Comma;
		case VK_OEM_PERIOD: return KEYBOARD_Period;
		case VK_OEM_7:      return KEYBOARD_Apostrophe;
		case VK_SNAPSHOT:   return KEYBOARD_PrintScreen;
		case VK_SCROLL:     return KEYBOARD_ScrollLock;
		case VK_PAUSE:      return KEYBOARD_PauseBreak;
		case VK_NUMPAD0:    return KEYBOARD_Numpad0;
		case VK_NUMPAD1:    return KEYBOARD_Numpad1;
		case VK_NUMPAD2:    return KEYBOARD_Numpad2;
		case VK_NUMPAD3:    return KEYBOARD_Numpad3;
		case VK_NUMPAD4:    return KEYBOARD_Numpad4;
		case VK_NUMPAD5:    return KEYBOARD_Numpad5;
		case VK_NUMPAD6:    return KEYBOARD_Numpad6;
		case VK_NUMPAD7:    return KEYBOARD_Numpad7;
		case VK_NUMPAD8:    return KEYBOARD_Numpad8;
		case VK_NUMPAD9:    return KEYBOARD_Numpad9;
		case VK_DIVIDE:     return KEYBOARD_Divide;
		case VK_MULTIPLY:   return KEYBOARD_Multiply;
		case VK_DECIMAL:    return KEYBOARD_Decimal;
		case VK_NUMLOCK:    return KEYBOARD_NumLock;
		default:            return KEYBOARD_Unknown;
	}
}

struct PLATFORM_State {
	WNDCLASSEX window_class;
	bool       is_running;
};

struct WINDOW_State {
	HWND handle;
};


const char* PLATFORM_CLASSNAME = "ManicPhotons_Engine_PlatformClassName";


LRESULT CALLBACK PLATFORM_EventHandler(HWND window_handle, UINT platform_event, WPARAM w_param, LPARAM l_param) {
	switch(platform_event) {
		case WM_CLOSE: {
			DestroyWindow(window_handle); // Sends WM_DESTROY
			return 0;
		}
		case WM_DESTROY: {
			PostQuitMessage(0); // Sends WM_QUIT
			return 0;
		}
		default: {
			return DefWindowProc(window_handle, platform_event, w_param, l_param);
		}
	}
}


struct PLATFORM_State* PLATFORM_Initiate(void) {
	HINSTANCE program_instance = (HINSTANCE)GetModuleHandle(NULL);
	if(program_instance == NULL) {
		PLATFORM_ReportError("PLATFORM_Initiate: Failed to acquire instance handle!");
		return NULL;
	}

	WNDCLASSEX window_class = {
		.cbSize        = sizeof(WNDCLASSEX),
		.style         = 0,
		.lpfnWndProc   = PLATFORM_EventHandler,
		.cbClsExtra    = 0,
		.cbWndExtra    = 0,
		.hInstance     = program_instance,
		.hIcon         = LoadIcon(NULL, IDI_APPLICATION),
		.hCursor       = LoadCursor(NULL, IDC_ARROW),
		.hbrBackground = (HBRUSH)(COLOR_WINDOW+1),
		.lpszMenuName  = NULL,
		.lpszClassName = PLATFORM_CLASSNAME,
		.hIconSm       = LoadIcon(NULL, IDI_APPLICATION)
	};
	if(!RegisterClassEx(&window_class)) {
		PLATFORM_ReportError("PLATFORM_Initiate: Failed to register window class!");
		return 0;
	}

	struct PLATFORM_State* platform = VirtualAlloc(NULL, sizeof(struct PLATFORM_State), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if(platform == NULL) {
		PLATFORM_ReportError("PLATFORM_Initiate: Failed to allocate platform state memory!");
		UnregisterClass(PLATFORM_CLASSNAME, program_instance);
		return NULL;
	}
	
	platform->window_class = window_class;
	return platform;
}

void PLATFORM_PrepareToRun(struct PLATFORM_State* platform) {
	if(!platform) return;
	platform->is_running = true;
}

bool PLATFORM_IsRunning(struct PLATFORM_State* platform) {
	if(!platform) return false;
	return platform->is_running;
}

void PLATFORM_ProcessPendingEvents(struct PLATFORM_State* platform, struct KEYBOARD_State* keyboard) {
	if(!platform) return;

	bool program_should_keep_running = true;
	MSG platform_event = {0};

	while(PeekMessage(&platform_event, NULL, 0, 0, PM_REMOVE)) {
		TranslateMessage(&platform_event);
		DispatchMessage(&platform_event);

		if(platform_event.message == WM_QUIT) {
			platform->is_running = false;
		}
		if(platform_event.message == WM_KEYDOWN) {
			uint8_t keycode = KEYBOARD_ConvertKeycode(platform_event.wParam);
			KEYBOARD_Press(keyboard, keycode);
		}
		if(platform_event.message == WM_KEYUP) {
			uint8_t keycode = KEYBOARD_ConvertKeycode(platform_event.wParam);
			KEYBOARD_Release(keyboard, keycode);
		}
	}
}

void PLATFORM_Terminate(struct PLATFORM_State** indirect_platform) {
	if(!indirect_platform) return;

	struct PLATFORM_State* platform = *indirect_platform;
	if(!platform) return;

	UnregisterClass(PLATFORM_CLASSNAME, platform->window_class.hInstance);
	VirtualFree(platform, 0, MEM_RELEASE);
	*indirect_platform = NULL;
}

void PLATFORM_ReportError(const char* context) {
	DWORD error_code = GetLastError();

	LPVOID message = NULL;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error_code,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPSTR)&message,
		0,
		NULL
	);

	MessageBox(NULL, message, context, MB_ICONEXCLAMATION | MB_OK);
	LocalFree(message);

}

void WINDOW_ReportError(const char* context) {
	DWORD error_code = GetLastError();

	LPVOID message = NULL;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error_code,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPSTR)&message,
		0,
		NULL
	);

	MessageBox(NULL, message, context, MB_ICONEXCLAMATION | MB_OK);
	LocalFree(message);

}

struct WINDOW_State* WINDOW_Initiate(struct PLATFORM_State* platform) {
	if(platform == NULL) {
		WINDOW_ReportError("WINDOW_Initiate: No platform state provided!");
		return NULL;
	}

	HWND window_handle = CreateWindowEx(
		WS_EX_CLIENTEDGE,
		platform->window_class.lpszClassName,
		"The title of my window",
		WS_CAPTION | WS_BORDER | WS_SYSMENU, // TODO: WS_SYSMENU is needed to provide a close button. Handling user inputs will replace this.
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		960,
		540,
		NULL,
		NULL,
		platform->window_class.hInstance,
		NULL
	);
	if(window_handle == NULL) {
		WINDOW_ReportError("WINDOW_Initiate: Failed to create a window handle!");
		return NULL;
	}

	struct WINDOW_State* window = VirtualAlloc(NULL, sizeof(struct WINDOW_State), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if(window == NULL) {
		WINDOW_ReportError("WINDOW_Initiate: Failed to allocate window state memory!");
		DestroyWindow(window_handle);
		return NULL;
	}

	window->handle = window_handle;
	return window;
}

bool WINDOW_MakeVisible(struct WINDOW_State* window) {
	if(!window) {
		WINDOW_ReportError("WINDOW_MakeVisible: No window state provided!");
		return false;
	}

	ShowWindow(window->handle, SW_NORMAL);
        if(!UpdateWindow(window->handle)) {
		WINDOW_ReportError("WINDOW_MakeVisible: Failed to update window!");
		return false;
	}

	return true;
}

bool WINDOW_MakeInvisible(struct WINDOW_State* window) {
	if(!window) {
		WINDOW_ReportError("WINDOW_MakeInvisible: No window state provided!");
		return false;
	}

	ShowWindow(window->handle, SW_HIDE);
        if(!UpdateWindow(window->handle)) {
		WINDOW_ReportError("WINDOW_MakeInvisible: Failed to update window!");
		return false;
	}

	return true;
}

void WINDOW_RequestDestroy(struct WINDOW_State* window) {
	if(!window) return;

	DestroyWindow(window->handle);
}

void WINDOW_Terminate(struct WINDOW_State** indirect_window) {
	if(!indirect_window) return;

	struct WINDOW_State* window = *indirect_window;
	if(!window) return;

	DestroyWindow(window->handle);
	VirtualFree(window, 0, MEM_RELEASE);
	*indirect_window = NULL;
}


