#include "../api/keyboard.h"
#include <stdlib.h>

struct KEYBOARD_State* KEYBOARD_Initiate(void) {
	struct KEYBOARD_State* keyboard = malloc(sizeof(struct KEYBOARD_State));
	if(keyboard == NULL) return NULL;

	for(uint8_t i = 0; i < KEYBOARD_KeyCount; ++i) {
		keyboard->keys[i].seconds  = 0.0f;
		keyboard->keys[i].is_down  = false;
		keyboard->keys[i].was_down = false;
	}

	return keyboard;
}

void KEYBOARD_Update(struct KEYBOARD_State* keyboard, float elapsed_seconds) {
	if(!keyboard) return;

	for(uint8_t i = 0; i < KEYBOARD_KeyCount; ++i) {
		keyboard->keys[i].seconds += elapsed_seconds;
		keyboard->keys[i].was_down = keyboard->keys[i].is_down;
	}
}

void KEYBOARD_Press(struct KEYBOARD_State* keyboard, uint8_t keycode) {
	if(!keyboard) return;
	if(keycode >= KEYBOARD_KeyCount) return;
	if(keyboard->keys[keycode].is_down == true) return;

	keyboard->keys[keycode].seconds = 0.0f;
	keyboard->keys[keycode].is_down = true;
}

void KEYBOARD_Release(struct KEYBOARD_State* keyboard, uint8_t keycode) {
	if(!keyboard) return;
	if(keycode >= KEYBOARD_KeyCount) return;
	if(keyboard->keys[keycode].is_down == false) return;

	keyboard->keys[keycode].seconds = 0.0f;
	keyboard->keys[keycode].is_down = false;
}

bool KEYBOARD_IsUp(struct KEYBOARD_State* keyboard, uint8_t keycode) {
	if(!keyboard) return false;
	if(keycode >= KEYBOARD_KeyCount) return false;

	return !(keyboard->keys[keycode].is_down);
}

bool KEYBOARD_IsDown(struct KEYBOARD_State* keyboard, uint8_t keycode) {
	if(!keyboard) return false;
	if(keycode >= KEYBOARD_KeyCount) return false;

	return keyboard->keys[keycode].is_down;
}

bool KEYBOARD_WasUp(struct KEYBOARD_State* keyboard, uint8_t keycode) {
	if(!keyboard) return false;
	if(keycode >= KEYBOARD_KeyCount) return false;

	return !(keyboard->keys[keycode].was_down);
}

bool KEYBOARD_WasDown(struct KEYBOARD_State* keyboard, uint8_t keycode) {
	if(!keyboard) return false;
	if(keycode >= KEYBOARD_KeyCount) return false;

	return keyboard->keys[keycode].was_down;
}

float KEYBOARD_GetDuration(struct KEYBOARD_State* keyboard, uint8_t keycode) {
	if(!keyboard) return 0.0f;
	if(keycode >= KEYBOARD_KeyCount) return 0.0f;
	return keyboard->keys[keycode].seconds;
}

void KEYBOARD_Terminate(struct KEYBOARD_State** indirect_keyboard) {
	if(!indirect_keyboard) return; 

	struct KEYBOARD_State* keyboard = *indirect_keyboard;
	if(!keyboard) return;

	for(uint8_t i = 0; i < KEYBOARD_KeyCount; ++i) {
		keyboard->keys[i].seconds  = 0.0f;
		keyboard->keys[i].is_down  = false;
		keyboard->keys[i].was_down = false;
	}
	free(keyboard);
	*indirect_keyboard = NULL;
}




