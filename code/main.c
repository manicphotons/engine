#include "../api/platform.h"
#include "../api/window.h"
#include "../api/keyboard.h"
#include "../api/timer.h"

int main(void) {
	struct PLATFORM_State* platform = PLATFORM_Initiate();
	if(!platform) {
		return -1;
	}

	struct WINDOW_State* window = WINDOW_Initiate(platform);
	if(!window) {
		PLATFORM_Terminate(&platform);
		return -1;
	}

	struct KEYBOARD_State* keyboard = KEYBOARD_Initiate();
	if(!keyboard) {
		WINDOW_Terminate(&window);
		PLATFORM_Terminate(&platform);
		return -1;
	}

	struct TIMER_State main_timer = {0};
	TIMER_Initiate(&main_timer);

	if(!WINDOW_MakeVisible(window)) {
		WINDOW_Terminate(&window);
		PLATFORM_Terminate(&platform);
		return -1;
	}

	PLATFORM_PrepareToRun(platform);
	while(PLATFORM_IsRunning(platform)) {
		TIMER_Update(&main_timer);
		KEYBOARD_Update(keyboard, main_timer.elapsed);
		PLATFORM_ProcessPendingEvents(platform, keyboard);

		if(KEYBOARD_IsDown(keyboard, KEYBOARD_LetterA) && (KEYBOARD_GetDuration(keyboard, KEYBOARD_LetterA) > 3.0f)) {
			WINDOW_RequestDestroy(window);
		}
	}
	
	KEYBOARD_Terminate(&keyboard);
	WINDOW_Terminate(&window);
	PLATFORM_Terminate(&platform);
	return 0;
}
