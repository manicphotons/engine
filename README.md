# The ManicPhotons Engine

## Description
This will be the ManicPhotons game engine.
I have several projects planned that will make use of it.
There is one game and multiple tools (used for its development).
The design of the engine is being guided by these.
As engine functionality is added, I will reveal the game and tool details.


## Installation
Currently I am only working in Windows 10.
I much prefer working in linux (under X11), so support will be added shortly.
In the meantime, open the "<project root>/make/W10" directory from a VS2022 command prompt.
Then simply run the required script (clean, make, run).


## Roadmap
I have over a decade's experience making engines and tools for the games industry.
Thus I have a pretty clear roadmap for writing a minimal viable product.
But I am still decided how to best outline this from an educational perspective.
Once I have the details in place I will provide an overview here.


## License
The project will be open source, but I am yet to decide upon the correct license.
When I do, I will make note of it here.


## Reference Links
- [msdn/win32](https://learn.microsoft.com/en-us/windows/win32/api/)
