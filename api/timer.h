#ifndef MPE_API_TIMER_H
#define MPE_API_TIMER_H

struct TIMER_State {
	double frequency;
	double previous;
	double current;
	double elapsed;
};

void TIMER_Initiate(struct TIMER_State* timer);
void TIMER_Update(struct TIMER_State* timer);
void TIMER_Terminate(struct TIMER_State* timer);

#endif//MPE_API_TIMER_H
