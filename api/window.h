#ifndef MPE_API_WINDOW_H
#define MPE_API_WINDOW_H

#include <stdbool.h>

struct PLATFORM_State;
struct WINDOW_State;

struct WINDOW_State* WINDOW_Initiate(struct PLATFORM_State* platform);
bool                 WINDOW_MakeVisible(struct WINDOW_State* window);
bool                 WINDOW_MakeInvisible(struct WINDOW_State* window);
void                 WINDOW_RequestDestroy(struct WINDOW_State* window);
void                 WINDOW_Terminate(struct WINDOW_State** indirect_window);

void WINDOW_ReportError(const char* context);

#endif//MPE_API_WINDOW_H
