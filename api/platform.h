#ifndef MPE_API_PLATFORM_H
#define MPE_API_PLATFORM_H

#include <stdbool.h>

struct PLATFORM_State;
struct KEYBOARD_State;

struct PLATFORM_State* PLATFORM_Initiate(void);
void                   PLATFORM_PrepareToRun(struct PLATFORM_State* platform);
bool                   PLATFORM_IsRunning(struct PLATFORM_State* platform);
void                   PLATFORM_ProcessPendingEvents(struct PLATFORM_State* platform, struct KEYBOARD_State* keyboard);
void                   PLATFORM_Terminate(struct PLATFORM_State** indirect_platform);

void PLATFORM_ReportError(const char* context);

#endif//MPE_API_PLATFORM_H
