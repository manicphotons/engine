#ifndef MPE_API_KEYBOARD_H
#define MPE_API_KEYBOARD_H

#include <stdbool.h>
#include <stdint.h>

#define KEYBOARD_Unknown        0  /* For all other keys */
#define KEYBOARD_Enter          1  /* VK_RETURN */
#define KEYBOARD_Escape         2  /* VK_ESCAPE */
#define KEYBOARD_Backspace      3  /* VK_BACK */
#define KEYBOARD_Tab            5  /* VK_TAB */
#define KEYBOARD_Spacebar       6  /* VK_SPACE */
#define KEYBOARD_CapsLock       7  /* VK_CAPITAL */
#define KEYBOARD_Insert         8  /* VK_INSERT */
#define KEYBOARD_Delete         4  /* VK_DELETE */
#define KEYBOARD_PageUp         9  /* VK_PRIOR */
#define KEYBOARD_PageDown      10 /* VK_NEXT */
#define KEYBOARD_Home          11 /* VK_HOME */
#define KEYBOARD_End           12 /* VK_END */
#define KEYBOARD_Up            13 /* VK_UP */
#define KEYBOARD_Down          14 /* VK_DOWN */
#define KEYBOARD_Left          15 /* VK_LEFT */
#define KEYBOARD_Right         16 /* VK_RIGHT */
#define KEYBOARD_LeftShift     17 /* VK_LSHIFT */
#define KEYBOARD_RightShift    18 /* VK_RSHIFT */
#define KEYBOARD_LeftControl   19 /* VK_LCONTROL */
#define KEYBOARD_RightControl  20 /* VK_RCONTROL */
#define KEYBOARD_LeftAlt       21 /* VK_LMENU */
#define KEYBOARD_RightAlt      22 /* VK_RMENU */
#define KEYBOARD_LeftSuper     23 /* VK_LWIN */
#define KEYBOARD_RightSuper    24 /* VK_RWIN */
#define KEYBOARD_LetterA       25 /* 0x41 */
#define KEYBOARD_LetterB       26 /* 0x42 */
#define KEYBOARD_LetterC       27 /* 0x43 */
#define KEYBOARD_LetterD       28 /* 0x44 */
#define KEYBOARD_LetterE       29 /* 0x45 */
#define KEYBOARD_LetterF       30 /* 0x46 */
#define KEYBOARD_LetterG       31 /* 0x47 */
#define KEYBOARD_LetterH       32 /* 0x48 */
#define KEYBOARD_LetterI       33 /* 0x49 */
#define KEYBOARD_LetterJ       34 /* 0x4A */
#define KEYBOARD_LetterK       35 /* 0x4B */
#define KEYBOARD_LetterL       36 /* 0x4C */
#define KEYBOARD_LetterM       37 /* 0x4D */
#define KEYBOARD_LetterN       38 /* 0x4E */
#define KEYBOARD_LetterO       39 /* 0x4F */
#define KEYBOARD_LetterP       40 /* 0x50 */
#define KEYBOARD_LetterQ       41 /* 0x51 */
#define KEYBOARD_LetterR       42 /* 0x52 */
#define KEYBOARD_LetterS       43 /* 0x53 */
#define KEYBOARD_LetterT       44 /* 0x54 */
#define KEYBOARD_LetterU       45 /* 0x55 */
#define KEYBOARD_LetterV       46 /* 0x56 */
#define KEYBOARD_LetterW       47 /* 0x57 */
#define KEYBOARD_LetterX       48 /* 0x58 */
#define KEYBOARD_LetterY       49 /* 0x59 */
#define KEYBOARD_LetterZ       50 /* 0x5A */
#define KEYBOARD_Number1       51 /* 0x31 */
#define KEYBOARD_Number2       52 /* 0x32 */
#define KEYBOARD_Number3       53 /* 0x33 */
#define KEYBOARD_Number4       54 /* 0x34 */
#define KEYBOARD_Number5       55 /* 0x35 */
#define KEYBOARD_Number6       56 /* 0x36 */
#define KEYBOARD_Number7       57 /* 0x37 */
#define KEYBOARD_Number8       58 /* 0x38 */
#define KEYBOARD_Number9       59 /* 0x39 */
#define KEYBOARD_Number0       60 /* 0x30 */
#define KEYBOARD_Function01    61 /* VK_F1 */
#define KEYBOARD_Function02    62 /* VK_F2 */
#define KEYBOARD_Function03    63 /* VK_F3 */
#define KEYBOARD_Function04    64 /* VK_F4 */
#define KEYBOARD_Function05    65 /* VK_F5 */
#define KEYBOARD_Function06    66 /* VK_F6 */
#define KEYBOARD_Function07    67 /* VK_F7 */
#define KEYBOARD_Function08    68 /* VK_F8 */
#define KEYBOARD_Function09    69 /* VK_F9 */
#define KEYBOARD_Function10    70 /* VK_F10 */
#define KEYBOARD_Function11    71 /* VK_F11 */
#define KEYBOARD_Function12    72 /* VK_F12 */
#define KEYBOARD_Function13    73 /* VK_F13 */
#define KEYBOARD_Function14    74 /* VK_F14 */
#define KEYBOARD_Function15    75 /* VK_F15 */
#define KEYBOARD_Function16    76 /* VK_F16 */
#define KEYBOARD_Function17    77 /* VK_F17 */
#define KEYBOARD_Function18    78 /* VK_F18 */
#define KEYBOARD_Function19    79 /* VK_F19 */
#define KEYBOARD_Function20    80 /* VK_F20 */
#define KEYBOARD_Function21    81 /* VK_F21 */
#define KEYBOARD_Function22    82 /* VK_F22 */
#define KEYBOARD_Function23    83 /* VK_F23 */
#define KEYBOARD_Function24    84 /* VK_F24 */
#define KEYBOARD_Backtick      86 /* VK_OEM_3 */
#define KEYBOARD_LeftSquare    87 /* VK_OEM_4 */
#define KEYBOARD_RightSquare   88 /* VK_OEM_6 */
#define KEYBOARD_ForeSlash     89 /* VK_OEM_2 */
#define KEYBOARD_BackSlash     90 /* VK_OEM_5 */
#define KEYBOARD_Equals        91 /* VK_ADD */
#define KEYBOARD_Subtract      92 /* VK_SUBTRACT */
#define KEYBOARD_SemiColon     93 /* VK_OEM_1 */
#define KEYBOARD_Comma         94 /* VK_OEM_COMMA */
#define KEYBOARD_Period        95 /* VK_OEM_PERIOD */
#define KEYBOARD_Apostrophe    96 /* VK_OEM_7 */
#define KEYBOARD_PrintScreen   97 /* VK_SNAPSHOT */
#define KEYBOARD_ScrollLock    98 /* VK_SCROLL */
#define KEYBOARD_PauseBreak    99 /* VK_PAUSE */
#define KEYBOARD_Numpad0      100 /* VK_NUMPAD0 */
#define KEYBOARD_Numpad1      101 /* VK_NUMPAD1 */
#define KEYBOARD_Numpad2      102 /* VK_NUMPAD2 */
#define KEYBOARD_Numpad3      103 /* VK_NUMPAD3 */
#define KEYBOARD_Numpad4      104 /* VK_NUMPAD4 */
#define KEYBOARD_Numpad5      105 /* VK_NUMPAD5 */
#define KEYBOARD_Numpad6      106 /* VK_NUMPAD6 */
#define KEYBOARD_Numpad7      107 /* VK_NUMPAD7 */
#define KEYBOARD_Numpad8      108 /* VK_NUMPAD8 */
#define KEYBOARD_Numpad9      109 /* VK_NUMPAD9 */
#define KEYBOARD_Divide       110 /* VK_DIVIDE */
#define KEYBOARD_Multiply     111 /* VK_MULTIPLY */
#define KEYBOARD_Decimal      112 /* VK_DECIMAL */
#define KEYBOARD_NumLock      113 /* VK_NUMLUCK */
#define KEYBOARD_KeyCount 114

// TODO: There are many keys not yet accounted for.
//       For example the media keys and the numpad keys for minus, plus and enter.
uint8_t KEYBOARD_ConvertKeycode(uint16_t code);


// TODO: Keys should report how long it has been in its current state.
//       Currently I don't know how to do this in a cache aware manner,
//       as I don't yet know the control scheme I'll use.
//       This method is extraordinarily cache inefficint.
//       It uses 64 bits per key, when we probably only need 2.
struct KEYBOARD_KeyState {
 	float seconds;
 	uint16_t is_down;
 	uint16_t was_down;
};

struct KEYBOARD_State {
	struct KEYBOARD_KeyState keys[KEYBOARD_KeyCount];
};

struct KEYBOARD_State* KEYBOARD_Initiate(void);
void                   KEYBOARD_Update(struct KEYBOARD_State* keyboard, float elapsed_seconds);
void                   KEYBOARD_Press(struct KEYBOARD_State* keyboard, uint8_t keycode);
void                   KEYBOARD_Release(struct KEYBOARD_State* keyboard, uint8_t keycode);
bool                   KEYBOARD_IsUp(struct KEYBOARD_State* keyboard, uint8_t keycode);
bool                   KEYBOARD_IsDown(struct KEYBOARD_State* keyboard, uint8_t keycode);
bool                   KEYBOARD_WasUp(struct KEYBOARD_State* keyboard, uint8_t keycode);
bool                   KEYBOARD_WasDown(struct KEYBOARD_State* keyboard, uint8_t keycode);
float                  KEYBOARD_GetDuration(struct KEYBOARD_State* keyboard, uint8_t keycode);
void                   KEYBOARD_Terminate(struct KEYBOARD_State** indirect_keyboard);

#endif//MPE_API_KEYBOARD_H


